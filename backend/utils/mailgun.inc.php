<?php
function send_mailgun($arr){
      $config = array();
      $config['api_key'] = "key-a22c9ffa2b5a26f149dfb6d0f6dd4cf6"; //API Key
      $config['api_url'] = "https://api.mailgun.net/v3/sandboxa13a728fefd7492ebf4c94a526f3ff14.mailgun.org/messages"; //API Base URL
  $subject = '';
  $body = '';
  $ruta = '';

  switch ($arr['type']) {
      case 'alta':
          $subject = 'Tu Alta en Project Events';
          $ruta = "<a href='http://".$_SERVER['HTTP_HOST']."/Project_Events-Angular/#/user/activar/" . $arr['token'] ."~".$arr['id']. "'>aqu&iacute;</a>";
          $body = 'Gracias por unirte a nuestra aplicaci&oacute;n. Para finalizar el registro, pulsa ' . $ruta;
          break;

      case 'modificacion':
          $subject = 'Tu Nuevo Password en Project Events';
          $ruta = "<a href='http://".$_SERVER['HTTP_HOST']."/Project_Events-Angular/#/user/cambiarpass/" . $arr['token'] . "'>aqu&iacute;</a>";
          $body = 'Para recordar tu password pulsa ' . $ruta;
          break;

      case 'contact':
          $subject = 'Tu Peticion Contact ha sido enviada';
          $ruta = '<a href="http://'.$_SERVER['HTTP_HOST'].'/Project_Events-Angular/#/"' . '>aqu&iacute;</a>';
          $body = 'Ha contactado con el soporte de Project Events, copia del mensaje enviado: <br><br> <strong>Asunto:</strong> '.$arr['inputSubject'].'<br><br> <strong>Mensaje:</strong> '.$arr['inputMessage'].'<br><br>Para visitar nuestra web, pulsa ' . $ruta;
          break;

      case 'admin':
          $subject = $arr['inputSubject'];
          $body = 'inputName: ' . $arr['inputName'] . '<br>' .
                  'inputEmail: ' . $arr['inputEmail'] . '<br>' .
                  'inputSubject: ' . $arr['inputSubject'] . '<br>' .
                  'inputMessage: ' . $arr['inputMessage'];
          break;
  }

  $message = array();
  $message['from'] = "josepsolerpla@gmail.com";
  $message['to'] = $arr['inputEmail'];
  $message['h:Reply-To'] = "josepsolerpla@gmail.com";
  $message['subject'] = $subject;
  $message['html'] = 'Hello ' . $arr['inputName'] . ',<br><br>'.$body;

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $config['api_url']);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  curl_setopt($ch, CURLOPT_POST, true); 
  curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}