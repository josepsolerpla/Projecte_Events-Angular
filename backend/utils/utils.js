function bake_cookie(name, value) {
  var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
  var cookie = [name, '=', JSON.stringify(value), '; path=/; domain=.', window.location.host.toString()].join('');
  document.cookie = cookie;
}
function read_cookie(name) {
 var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
 result && (result = JSON.parse(result[1]));
 return result;
}
function delete_cookie(name) {
  document.cookie = [name, '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/; domain=.', window.location.host.toString()].join('');
}
function petition({mod,fun,data = {}}) {                                                    /* PETITION */
    const promise = new Promise(function (resolve, reject) {
        $.ajax({
            url: "../../"+mod+"/"+fun,
            type: 'POST',
            data : {
                "data": data,   
            },
            success: function(response) { 
                resolve(response);
                return response;
            },
        })
        .fail(function() {
            reject();
           return "error"; 
        });
    })
    return promise;
}
function validate_exp(val,{leng = 3,regexp = /^[a-zA-Z0-9]*$/}) {
  if (val.length > leng) {
        return regexp.test(val);
    }
    return false;
}
function base64url(source) {
  // Encode in classical base64
  encodedSource = CryptoJS.enc.Base64.stringify(source);

  // Remove padding equal characters
  encodedSource = encodedSource.replace(/=+$/, '');

  // Replace characters according to base64url specifications
  encodedSource = encodedSource.replace(/\+/g, '-');
  encodedSource = encodedSource.replace(/\//g, '_');

  return encodedSource;
}
function amigable(url) {
    var link="http://localhost/Project_Events-Angular";
    url = url.replace("?", "");
    url = url.split("&");

    for (var i=0;i<url.length;i++) {
        var aux = url[i].split("=");
        link +=  "/"+aux[1];
    }
    link +="/";
    return link;
}