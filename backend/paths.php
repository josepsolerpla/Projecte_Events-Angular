<?php
//SITE_ROOT
$path1 = /*$_SERVER['DOCUMENT_ROOT'] . */'/Project_Events-Angular/backend/';
$path2 = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events-Angular/backend/';
define('SITE_ROOT', $path1);
define('SITE_ROOT_full', $path2);
//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/Project_Events-Angular/backend/');
define('SITE_PATH_full', 'https://' . $_SERVER['HTTP_HOST'] . '/Project_Events-Angular/backend/');
//CSS
define('CSS_PATH', SITE_PATH . 'view/css/');

//JS
define('JS_PATH', SITE_PATH . 'view/js/');

//IMG
define('IMG_PATH', SITE_PATH . 'view/img/');

//log
define('USER_LOG_DIR', SITE_ROOT . 'log/user/Site_User_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . 'log/general/Site_General_errors.log');

define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT_full . 'model/');
//view
define('VIEW_PATH_INC', SITE_ROOT_full . 'view/inc/');
define('VIEW_PATH_INC_ERROR', SITE_ROOT_full . 'view/inc/templates_error/');
//modules
define('MODULES_PATH', SITE_ROOT_full . 'modules/');
//resources
define('RESOURCES', SITE_ROOT_full . 'resources/');
//media
define('MEDIA_PATH', SITE_ROOT . 'media/');
//utils
define('UTILS', SITE_ROOT_full . 'utils/');
define('UTILS_nofull', SITE_ROOT . 'utils/');

//model homepage
define('UTILS_HOMEPAGE', SITE_ROOT . 'modules/homepage/utils/');
define('HOMEPAGE_JS_PATH', SITE_PATH . 'modules/homepage/view/js/');
define('MODEL_PATH_HOMEPAGE', SITE_ROOT . 'modules/homepage/model/');
define('DAO_HOMEPAGE', SITE_ROOT . 'modules/homepage/model/DAO/');
define('BLL_HOMEPAGE', SITE_ROOT . 'modules/homepage/model/BLL/');
define('MODEL_HOMEPAGE', SITE_ROOT . 'modules/homepage/model/model/');
define('VIEW_HOMEPAGE', SITE_ROOT . 'modules/homepage/view/');

//model events
define('UTILS_EVENTS', SITE_ROOT . 'modules/events/utils/');
define('EVENTS_JS_PATH', SITE_PATH . 'modules/events/view/js/');
define('MODEL_PATH_EVENTS', '/var/www/html'. SITE_ROOT . 'modules/events/model/');
define('DAO_EVENTS', '/var/www/html'. SITE_ROOT . 'modules/events/model/DAO/');
define('BLL_EVENTS', '/var/www/html'. SITE_ROOT . 'modules/events/model/BLL/');
define('MODEL_EVENTS', '/var/www/html'. SITE_ROOT . 'modules/events/model/model/');
define('VIEW_EVENTS', SITE_ROOT . 'modules/events/view/');

//model contact
define('UTILS_CONTACT', SITE_ROOT . 'modules/contact/utils/');
define('CONTACT_JS_PATH', SITE_PATH . 'modules/contact/view/js/');
define('MODEL_PATH_CONTACT', SITE_ROOT . 'modules/contact/model/');
define('DAO_CONTACT', SITE_ROOT . 'modules/contact/model/DAO/');
define('BLL_CONTACT', SITE_ROOT . 'modules/contact/model/BLL/');
define('MODEL_CONTACT', SITE_ROOT . 'modules/contact/model/model/');
define('VIEW_CONTACT', SITE_ROOT . 'modules/contact/view/');

//model login
define('UTILS_LOGIN', SITE_ROOT . 'modules/login/utils/');
define('LOGIN_JS_PATH', SITE_PATH . 'modules/login/view/js/');
define('MODEL_PATH_LOGIN', SITE_ROOT . 'modules/login/model/');
define('DAO_LOGIN', SITE_ROOT . 'modules/login/model/DAO/');
define('BLL_LOGIN', SITE_ROOT . 'modules/login/model/BLL/');
define('MODEL_LOGIN', SITE_ROOT . 'modules/login/model/model/');
define('VIEW_LOGIN', SITE_ROOT . 'modules/login/view/');
define('JS_LOGIN', SITE_ROOT . 'modules/login/view/js/');

//model users
define('UTILS_USERS', SITE_ROOT . 'modules/users/utils/');
define('USERS_JS_PATH', SITE_PATH . 'modules/users/view/js/');
define('MODEL_PATH_USERS', SITE_ROOT . 'modules/users/model/');
define('DAO_USERS', SITE_ROOT . 'modules/users/model/DAO/');
define('BLL_USERS', SITE_ROOT . 'modules/users/model/BLL/');
define('MODEL_USERS', SITE_ROOT_full . 'modules/users/model/model/');
define('VIEW_USERS', SITE_ROOT . 'modules/users/view/');
define('JS_USERS', SITE_ROOT . 'modules/users/view/js/');


//amigables
define('URL_AMIGABLES', TRUE);
