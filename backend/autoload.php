<?php
spl_autoload_register(null, false);
spl_autoload_extensions('.php,.inc.php,.class.php,.class.singleton.php');

spl_autoload_register('loadClasses');
function loadClasses($className) {
    $porciones = explode("_", $className);
    $module_name = $porciones[0];
    $model_name = "";
    
    if(isset($porciones[1])){
        $model_name = $porciones[1];
        $model_name = strtoupper($model_name);
    }
        if (file_exists('/var/www/html/Project_Events-Angular/backend/modules/' . $module_name . '/model/'.$model_name.'/' . $className . '.class.singleton.php')) {//require(BLL_USERS . "user_bll.class.singleton.php");
            set_include_path('/var/www/html/Project_Events-Angular/backend/modules/' . $module_name . '/model/'.$model_name.'/');
            spl_autoload($className);
        }
        
        elseif (file_exists('model/' . $className . '.class.singleton.php')) {//require(MODEL_PATH . "db.class.singleton.php");
            set_include_path('model/');
            spl_autoload($className);
        }
        elseif (file_exists('classes/' . $className . '.class.singleton.php')) {//require(MODEL_PATH . "db.class.singleton.php");
            set_include_path('classes/');
            spl_autoload($className);
        }
    
}
