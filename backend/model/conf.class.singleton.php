<?php
	/*$path=$_SERVER['DOCUMENT_ROOT'].'/Project_Events-Angular/';
	define('SITE_ROOT',$path);
	define('MODEL_PATH',SITE_ROOT.'model/');*/
	class conf{
		private $_userdb;
        private $_passdb;
        private $_hostdb;
        private $_db;
        static $_instance;

        private function __construct() {
            $cnfg = parse_ini_file("/var/www/html/Project_Events-Angular/backend/model/bd.ini");
            $this->_userdb = $cnfg['user'];
            $this->_passdb = $cnfg['password'];
            $this->_hostdb = $cnfg['host'];
            $this->_db = $cnfg['db'];
        }

        private function __clone() {

        }
        //If its already created return it if not creat it
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function __get($prty) {
        	if(property_exists($this, $prty)){
        		return $this->$prty;
        	}
        }
	}