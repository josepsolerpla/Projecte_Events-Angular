<?php
	Class db {
		private $server;
        private $user;
        private $password;
        private $database;
        private $link;
        private $stmt;
        private $array;
        static $_instance;

        private function __construct() {
        	$this->setConexion();
        	$this->connect();
        }

        private function __clone() {
        	
        }
        //function to stablish the conexion used on __construct
        private function setConexion(){
        	require_once 'conf.class.singleton.php';
        	$conf = conf::getInstance();
            $this->server = $conf->_hostdb;
            $this->database = $conf->_db;
            $this->user = $conf->_userdb;
            $this->password = $conf->_passdb;
        }
        //If its already created return it if not creat it
        public static function getInstance() {
        	if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }

        public function connect() {
        	$this->link = new mysqli($this->server, $this->user, $this->password);
            $this->link->select_db($this->database);
        }
        //Function used to consult de database 
        public function execute($sql) {
        	$this->stmt = $this->link->query($sql);
            return $this->stmt;
        }

        public function list_db($db,$sql) {
            $table = $db->execute($sql);
            foreach ($table as $row) {$form[] = array_merge($row);};
                
            if (isset($form)) {
              return $form;
            }else
              return "not exist";

        	/*$this->array = array();
            while ($row = $stmt->fetch_array(MYSQLI_ASSOC)) {
                array_push($this->array, $row);
            }
            return $this->array;*/
        }

	}