-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: Project_Events
-- ------------------------------------------------------
-- Server version 5.7.22-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `state` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` VALUES (1,'josepsolerpla@gmail.com',1),(16,'jpspsp@gmail.com',0);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activate_user`
--

DROP TABLE IF EXISTS `activate_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activate_user` (
  `id_user` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activate_user`
--

LOCK TABLES `activate_user` WRITE;
/*!40000 ALTER TABLE `activate_user` DISABLE KEYS */;
INSERT INTO `activate_user` VALUES ('32','19c5ccbc27fff0bb758fdfd103173146'),('33','1e5a4157efda5f18c03f1d11fe6c210c');
/*!40000 ALTER TABLE `activate_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_info`
--

DROP TABLE IF EXISTS `events_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_info` (
  `id_event` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text,
  `propietario` text,
  `dirigido_a` text,
  `presupuesto` text,
  `fecha` date DEFAULT NULL,
  `country` text,
  `province` text,
  `poblac` text,
  `ticket_price` int(11) DEFAULT '0',
  `img_event` text,
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_event`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_info`
--

LOCK TABLES `events_info` WRITE;
/*!40000 ALTER TABLE `events_info` DISABLE KEYS */;
INSERT INTO `events_info` VALUES (1,'asd1','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(2,'asd2','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(3,'asd3','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(4,'asd4','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(5,'asd5','asdasd','+12','255','2018-03-21','AX','Pontevedra','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(6,'asdasd','asdasd','+12','255','2018-03-21','AX','Madrid','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(7,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(8,'asdasd','asdasd','+12','255','2018-03-21','AX','Andalucia','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(9,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(10,'asdasd','asdasd','+12','255','2018-03-21','AX','Barcelona','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(11,'asdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(12,'Diania','asdasd','+12','255','2018-03-21','AX','Valencia','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(13,'Dasdasd','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(14,'Dlkjlkj','asdasd','+12','255','2018-03-21','AX','Barcelona','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(15,'lkjlkj','asdasd','+12','255','2018-03-21','AX','','',25,'/Project_Events/media/default-avatar.png',NULL,NULL),(16,'Madridtest','madrid','+12','69000â‚¬','2018-03-26','ES','Madrid','Madrid',25,'/Project_Events/media/890214794-test.jpeg',NULL,NULL),(17,'Madridtest2','algu','+12','25000','2018-03-26','ES','Madrid','Abetos, Los (moralzarzal)',15,'/Project_Events/media/default-avatar.png',NULL,NULL);
/*!40000 ALTER TABLE `events_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_tickets`
--

DROP TABLE IF EXISTS `events_tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_tickets` (
  `id_event` int(11) DEFAULT NULL,
  `code_ticket` varchar(255) NOT NULL,
  `id_client` int(11) DEFAULT NULL,
  PRIMARY KEY (`code_ticket`),
  KEY `id_event` (`id_event`),
  KEY `id_client` (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_tickets`
--

LOCK TABLES `events_tickets` WRITE;
/*!40000 ALTER TABLE `events_tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `identify_accounts`
--

DROP TABLE IF EXISTS `identify_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `identify_accounts` (
  `id_account` int(255) DEFAULT NULL,
  `id_user` varchar(255) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `from` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `identify_accounts`
--

LOCK TABLES `identify_accounts` WRITE;
/*!40000 ALTER TABLE `identify_accounts` DISABLE KEYS */;
INSERT INTO `identify_accounts` VALUES (1,'1',NULL,'normal'),(1,'2',NULL,'normal'),(1,'3',NULL,'normal'),(1,'16',NULL,'normal'),(1,'159573670','1','social'),(1,'2068372189856716','1','social'),(16,'33','1','normal');
/*!40000 ALTER TABLE `identify_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_login`
--

DROP TABLE IF EXISTS `reg_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_login` (
  `id_user` varchar(250) NOT NULL,
  `date` varchar(45) DEFAULT NULL,
  `token` varchar(45) DEFAULT NULL,
  `val` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_login`
--

LOCK TABLES `reg_login` WRITE;
/*!40000 ALTER TABLE `reg_login` DISABLE KEYS */;
INSERT INTO `reg_login` VALUES ('1','2018-05-27 13:36:44','0842974d7a350dbfd843a8fc1c0f0b51','true'),('159573670','2018-05-26 12:56:58','c4f3d3c7180f1d420d970e03d033e218','true'),('19','0acb41a0f92629cc136fa12004dc7fe0','2018-05-27 14:18:23','true'),('2','2018-05-24 18:29:10','490192a62741e030254d0562833b3ef0','true'),('2068372189856716','2018-05-27 20:28:04','8458a44ef415b9418b94b0ef3c26387d','true'),('25','2018-05-27 20:17:15','6522eef7f3739c0564dfda1aa904ba07','true'),('31','2018-05-27 20:24:55','7238c5e44a4b7b12d065958086526743','true'),('32','19c5ccbc27fff0bb758fdfd103173146','2018-05-27 20:27:03','true'),('33','1e5a4157efda5f18c03f1d11fe6c210c','2018-05-27 20:43:43','true');
/*!40000 ALTER TABLE `reg_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_like`
--

DROP TABLE IF EXISTS `user_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_like` (
  `id_user` varchar(45) NOT NULL,
  `id_event` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_like`
--

LOCK TABLES `user_like` WRITE;
/*!40000 ALTER TABLE `user_like` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` text,
  `surname` text,
  `email` text,
  `phone` text,
  `password` text,
  `birthday` date DEFAULT NULL,
  `user_type` text,
  `photo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` VALUES (1,'admin','admin','josepsolerpla@gmail.com','+34123456789','admin','1998-07-28','admin',NULL),(2,'user','user','josepsolerpla@gmail.com','+34123456788','user','1998-07-28','user',NULL),(3,'owner','owner','josepsolerpla@gmail.com','+34987654321','owner','1998-07-28','owner',NULL),(16,'Yuse2','Soler','josepsolerpla@gmail.com','+34963258741','josep','2018-02-27','user',NULL),(33,'test',NULL,'jpspsp@gmail.com',NULL,'asd',NULL,'user',NULL);
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `Project_Events`.`create_account` AFTER INSERT ON `users_info` FOR EACH ROW
BEGIN
  DECLARE id_ varchar(255);
    
    IF EXISTS(SELECT * FROM accounts WHERE email = NEW.email) THEN
    SET id_ = (SELECT id FROM accounts WHERE email = NEW.email);
        INSERT INTO identify_accounts VALUES(id_,NEW.id,false,"normal");
  ELSE
        INSERT INTO `Project_Events`.`accounts` (`email`, `state`) VALUES (NEW.email, '0');
        SET id_ = (SELECT id FROM accounts WHERE email = NEW.email);
        INSERT INTO identify_accounts VALUES(id_,NEW.id,true,"normal");
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `users_info_FB`
--

DROP TABLE IF EXISTS `users_info_FB`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info_FB` (
  `id_user` varchar(255) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info_FB`
--

LOCK TABLES `users_info_FB` WRITE;
/*!40000 ALTER TABLE `users_info_FB` DISABLE KEYS */;
INSERT INTO `users_info_FB` VALUES ('159573670','Yuse','josep_soler_pla','','http://pbs.twimg.com/profile_images/483616614672510976/X5Eoqnsa_normal.png','twitter','user'),('2068372189856716','Josep','Soler','josepsolerpla@gmail.com','https://lookaside.facebook.com/platform/profilepic/?asid=2068372189856716&height=100&width=100&ext=1527593078&hash=AeT92rqEjBTx3a1O','facebook','user');
/*!40000 ALTER TABLE `users_info_FB` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `Project_Events`.`createAccount` AFTER INSERT ON `users_info_FB` FOR EACH ROW
BEGIN
  DECLARE id_ varchar(255);
    IF EXISTS(SELECT * FROM accounts WHERE email = NEW.email) THEN
    SET id_ = (SELECT id FROM accounts WHERE email = NEW.email);
        INSERT INTO identify_accounts VALUES(id_,NEW.id_user,true,"social");
  ELSE
        INSERT INTO `Project_Events`.`accounts` (`email`, `state`) VALUES (NEW.email, '0');
        SET id_ = (SELECT id FROM accounts WHERE email = NEW.email);
        INSERT INTO identify_accounts VALUES(id_,NEW.id_user,true,"social");
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `Project_Events`.`deleteIdentify` AFTER DELETE ON `users_info_FB` FOR EACH ROW
BEGIN
  DELETE FROM identify_accounts WHERE id_user = OLD.id_user;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Temporary table structure for view `users_infoallow`
--

DROP TABLE IF EXISTS `users_infoallow`;
/*!50001 DROP VIEW IF EXISTS `users_infoallow`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `users_infoallow` AS SELECT 
 1 AS `id`,
 1 AS `name`,
 1 AS `surname`,
 1 AS `email`,
 1 AS `photo`,
 1 AS `user_type`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'Project_Events'
--

--
-- Dumping routines for database 'Project_Events'
--
/*!50003 DROP PROCEDURE IF EXISTS `delete_account` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_account`(IN id_ varchar(255))
BEGIN
  DELETE FROM `Project_Events`.`users_info` WHERE `id` IN (SELECT id_user FROM Project_Events.identify_accounts WHERE id_account = id_ AND `from` = "normal" );
  DELETE FROM `Project_Events`.`users_info_FB` WHERE `id_user` IN (SELECT id_user FROM Project_Events.identify_accounts WHERE id_account = id_ AND `from` = "social" );
  DELETE FROM Project_Events.identify_accounts WHERE id_account = id_;
    DELETE FROM Project_Events.accounts WHERE id = id_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAllUsers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAllUsers`(IN id_ varchar(255))
BEGIN
    SELECT uia.* FROM accounts a 
    INNER JOIN 
    identify_accounts ia on a.id = ia.id_account 
    INNER JOIN 
    users_infoallow uia on ia.id_user=uia.id
  WHERE a.id = (SELECT id_account FROM identify_accounts WHERE id_user = id_);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `login_reg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `login_reg`(IN id_ varchar(255),IN tok_ varchar(255))
BEGIN
  IF EXISTS(SELECT * FROM reg_login WHERE id_user=id_) THEN
    UPDATE reg_login SET token=tok_,date=now() WHERE id_user=id_;
  ELSE 
    INSERT INTO reg_login VALUES(id_,tok_,now(),"true");
    END IF;
  SELECT * FROM users_infoallow WHERE id = id_;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `read_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `read_user`(IN id_u varchar(255),IN tok varchar(255))
BEGIN
  IF EXISTS(SELECT * FROM reg_login WHERE id_user=id_u AND token = tok) THEN
    SELECT * FROM users_infoallow WHERE id=id_u;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `users_infoallow`
--

/*!50001 DROP VIEW IF EXISTS `users_infoallow`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `users_infoallow` AS select `users_info`.`id` AS `id`,`users_info`.`name` AS `name`,`users_info`.`surname` AS `surname`,`users_info`.`email` AS `email`,`users_info`.`photo` AS `photo`,`users_info`.`user_type` AS `user_type` from `users_info` union select `users_info_FB`.`id_user` AS `id_user`,`users_info_FB`.`name` AS `name`,`users_info_FB`.`last_name` AS `last_name`,`users_info_FB`.`email` AS `email`,`users_info_FB`.`photo` AS `photo`,`users_info_FB`.`type` AS `user_type` from `users_info_FB` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-28 12:02:01