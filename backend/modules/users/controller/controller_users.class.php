<?php
class controller_users {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "users";
    }

    function insert_into() {
      $arrArgument = $_POST;
      $arrArgument['token'] = bin2hex(openssl_random_pseudo_bytes(16));
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "create_users", $arrArgument);
      if ($arrValue["exists"] == true) {
          $arrArgument = array(
            'id' => $arrValue['user']['id'],
            'type' => 'alta',
            'token' => $arrArgument['token'],
            'inputName' => $_POST['inputUsername'],
            'inputEmail' => $_POST['inputEmail'],
          );
          set_error_handler('ErrorHandler');
          try{
            $json = send_mailgun($arrArgument);
            $arrValue["exists"] = true;
            $arrValue["mssg"] = "Tu mensaje ha sido enviado correctamente";
          } catch (Exception $e) {
            $arrValue["exists"] = false;
            $arrValue["mssg"] = "Error en el servidor. Intentelo más tarde...";
          }
          restore_error_handler();
      }else
          echo "false|Error not exists";
      echo json_encode($arrValue);
    }
    function insert_into_firebase() {
      $arrArgument = $_POST;
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "create_users_firebase", $arrArgument);
      if ($arrValue['exists']) {
        $token = bin2hex(openssl_random_pseudo_bytes(16));
        $user = $arrValue['item'][0];
        $data = array("type"=>"normal","token" => $token , "id" => $user['id_user']);
        $arrValue = loadModel($path_model, "users_model", "select_user", $data);
        
        $back = array("token"=>$token,"user"=>$user,"exists"=>true);
        echo json_encode($back);
      }else{
        echo "error";
      }
    }
    function update_user(){
      $arrArgument = array("table"=>"users_info","change"=>$_POST['change'],'id'=>$_POST['id']);
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "update_user", $arrArgument);
      echo json_encode($arrValue);
    }
    function activar() {
      $arrArgument = $_POST;
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "activar", $arrArgument);
      echo json_encode($arrValue);
    }
    function get_account() {
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrArgument['call'] = "getAllUsers($_POST[id])";
      $arrValue = loadModel($path_model, "users_model", "CALL", $arrArgument);
      echo json_encode($arrValue);
    }
    function get_user(){
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrArgument['call'] = "profile_info($_POST[id])";
      $arrValue = loadModel($path_model, "users_model", "CALL", $arrArgument);
      echo json_encode($arrValue[0]);
    }
    function select_user() {
      $data = $_POST['search_factor'];
      
      $arrArgument = array("type"=>"normal","data"=>$data,"table"=>$_POST['table']);
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "select_user", $arrArgument);

      echo json_encode($arrValue);
    }
    function check_exist() {
      $data = $_POST['search_factor'];
        if ($_POST['logs']) {
         $logs = $_POST['logs'];
         unset($_POST['logs']);
        }
      $arrArgument = array("type"=>"password","data"=>$data,"table"=>"users_info");
      $path_model = MODEL_USERS;
      $arrValue = loadModel($path_model, "users_model", "select_user", $arrArgument);
      if ($arrValue["exists"] == true) {
        if ($logs == "true") {
          $token = bin2hex(openssl_random_pseudo_bytes(16));

          $data = array("token" => $token , "id" => $arrValue['user']['id'],"type"=>"token");
          $arrValue = loadModel($path_model, "users_model", "select_user", $data);
          echo json_encode($arrValue);
        }else
          echo json_encode($arrValue);
      }else {
        echo json_encode($arrValue);
      }
    }
    function validate_key() {
      $data = $_POST['search_factor'];

      $path_model = MODEL_USERS;

      $arrArgument = array("type"=>"normal","data"=>$data,"table"=>"identify_accounts");
      $arrValue = loadModel($path_model, "users_model", "select_user", $arrArgument);
      
      $data = array("id_account"=>$arrValue['user'][0]['id_account'],"token"=>$_POST['token']);
      $arrArgument = array("type"=>"normal","data"=>$_POST['search_factor'],"table"=>"reg_login");
      $arrValue = loadModel($path_model, "users_model", "select_user", $arrArgument);
      if ($arrValue["exists"] == true) {
        echo json_encode($arrValue['exists']);
      }else
        echo "false";
    }
    function giverate() {
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrArgument['call'] = "giverate($_POST[id_user],$_POST[id_event],$_POST[rating])";
     
      $arrValue = loadModel($path_model, "users_model", "CALL", $arrArgument);
      echo json_encode($arrValue);
    }
}
