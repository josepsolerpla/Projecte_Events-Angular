<?php
class users_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = users_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_users_BLL($arrArgument){
      return $this->dao->create_users_DAO($this->db, $arrArgument);
    }
    public function create_users_firebase_BLL($arrArgument){
      return $this->dao->create_users_firebase_DAO($this->db, $arrArgument);
    }
    public function update_user_BLL($arrArgument) {
        return $this->dao->update_user_DAO($this->db, $arrArgument);
    }
    public function check_exist_BLL($arrArgument){
        $type = "count";
        return $this->dao->check_exist_DAO($this->db, $arrArgument,$type);
    }
    public function read_user_BLL($arrArgument){
        return $this->dao->read_user_DAO($this->db, $arrArgument);
    }
    public function CALL_BLL($arrArgument){
        return $this->dao->CALL_DAO($this->db, $arrArgument);
    }

    public function log_login_BLL($arrArgument){
        return $this->dao->log_login_DAO($this->db, $arrArgument);
    }
    
    public function select_user_BLL($arrArgument){
        return $this->dao->select_user_DAO($this->db,$arrArgument);
    }
    public function activar_BLL($arrArgument){
        return $this->dao->activar_DAO($this->db,$arrArgument);
    }
}