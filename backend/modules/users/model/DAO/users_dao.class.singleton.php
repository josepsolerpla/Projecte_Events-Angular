<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class users_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_users_DAO($db, $arrArgument) {
        $name      = $arrArgument['inputUsername'];
        $email     = $arrArgument['inputEmail'];
        $password  = $arrArgument['inputPassword'];
        $user_type = "user";
        
        $sql   = "INSERT INTO users_info (`name`, `email`, `password`, `user_type`) VALUES ('$name','$email','$password','$user_type');";
          $exits = $db->execute($sql);
        $sql   = "SELECT * FROM users_info WHERE name='$name' AND password='$password'";
          $user  = $db->list_db($db,$sql);
        $sql   = "INSERT INTO activate_user (`id_user`,`token`) VALUES ('".$user[0]['id']."','".$arrArgument['token']."')";
          $activate_token = $db->execute($sql);
        $back = array("exists"=>$exits,"user"=>$user[0],"token"=>$activate_token);
        return $back;
    }
    public function create_users_firebase_DAO($db, $arrArgument) {
        $id      = $arrArgument['id'];
        $name    = $arrArgument['name'];
        $surname = $arrArgument['last_name'];
        $email   = $arrArgument['email'];
        $photo   = $arrArgument['photo'];
        $method  = $arrArgument['method'];
        
        $sql = "SELECT * FROM users_info_FB WHERE id_user = '$id'";
        
        $item  = $db->list_db($db,$sql);
        $exits = $db->execute($sql)->num_rows;
        
        if ($exits == 0) {
          $sql   = "INSERT INTO users_info_FB VALUES ('$id', '$name', '$surname','$email', '$photo', '$method', 'user')";
          $value = $db->execute($sql);
          $sql   = "SELECT * FROM users_info_FB WHERE id_user = '$id'";
          $item  = $db->list_db($db,$sql);   
        }else {
          $value = "exists";
        } 
        
        $back = array("exists" => $value , "item" => $item);
        
        return($back);       
    }
    public function update_user_DAO($db,$arrArgument){
      $e = 0;
      $sql = "UPDATE $arrArgument[table] ";
      foreach($arrArgument['change'] as $key => $val){
        if ($e == 0) {
          $sql .= "SET $key = '$val' ";
          $e++;
        }else{
          $sql .= ", $key = '$val' ";
        }
      }

      $sql .= "WHERE id = $arrArgument[id]";
      $value = $db->execute($sql);

      return $value;
    }
    public function CALL_DAO($db,$arrArgument){
      $sql  = "CALL $arrArgument[call]";
      $user = $db->list_db($db,$sql);
      return $user;
    }
    public function select_user_DAO($db,$arrArgument) {
      if ($arrArgument['type']) {
        switch ($arrArgument['type']) {
          case 'password':
            $name = $arrArgument['data']['name'];
            $sql  = "SELECT * FROM $arrArgument[table] WHERE name = '$name'";
            $rows = $db->execute($sql)->num_rows;
            if ($rows > 0) {
              $password = $arrArgument['data']['password'];
              $sql      = "SELECT * FROM $arrArgument[table] WHERE name = '$name' AND password = '$password'";
              $rows     = $db->execute($sql)->num_rows;
              if ($rows > 0) {
                $user      = $db->list_db($db,$sql);
                $exists    = true;
                $something = array("exists"=>true,"rows"=>$rows,"user"=>$user[0]);
              }else {
                $something = array("exists"=>false,"error"=>"wrong password");
              }
              
            }else {
              $something = array("exists"=>false,"error"=>"not exists");
            }

            return $something;
            
            break;
          case 'normal':

            $sql = "SELECT * FROM $arrArgument[table] ";
            $e = 0;
            if (!empty($arrArgument['data'])) {
              foreach($arrArgument['data'] as $key => $val){
                if ($e == 0) {
                  $sql .= "WHERE $key LIKE '$val' ";
                  $e++;
                }else{
                  $sql .= "AND $key LIKE '$val' ";
                }
              }
            }
            $rows = $db->execute($sql)->num_rows;
            if ($rows > 0) {
              $user = $db->list_db($db,$sql);
              $exists = true;
              $something = array("exists"=>true,"rows"=>$rows,"user"=>$user);
            }else {
              $something = array("exists"=>false,"error"=>"not exists");
            }

            return $something;

            break;
          case 'token':

            if ($arrArgument['id']) {
              $sql = "CALL login_reg('$arrArgument[id]','$arrArgument[token]')";
              $user = $db->list_db($db,$sql)[0];
              if ($user != "not exist") {
                $something = array("exists"=>true,"user"=>$user,"token"=>$arrArgument['token']);
              }else{
                $something = array("exists"=>false,'error'=>"not exists");
              }
            }else{
              echo "ERROR no ID";die;
            }
            return $something;

            break;
          default:
            echo "fail|ERROR no exist";die;
            break;
        }
      }else {
        echo "fail|ERROR no type function";die;
      }
    }
    public function activar_DAO($db,$arrArgument){
      $token = $arrArgument['token'];
      $id = $arrArgument['id_user'];
      $sql = "SELECT * FROM activate_user WHERE token = '$token' AND id_user = '$id'";
        $exits = $db->execute($sql)->num_rows;
        if ($exits != 0) {
          $back['success'] = true;
          $sql = "CALL login_reg('$id','$token')";
            $user = $db->list_db($db,$sql);
          $sql = "DELETE FROM activate_user WHERE token = '$token' AND id_user = '$id'";
          $delete = $db->execute($sql);
          $back['user'] = $user[0];
          $back['token'] = $token;
        }else{
          $back['datos'] = 503;
          $back['success'] = false;
        }
      return $back;
    }
}
