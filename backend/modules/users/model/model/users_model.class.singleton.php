<?php
class users_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = users_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    
    public function create_users($arrArgument) {
        return $this->bll->create_users_BLL($arrArgument);
    }
    public function create_users_firebase($arrArgument) {
        return $this->bll->create_users_firebase_BLL($arrArgument);
    }
    public function update_user($arrArgument){
        return $this->bll->update_user_BLL($arrArgument);
    }
    public function check_exist($arrArgument){
        return $this->bll->check_exist_BLL($arrArgument);
    }
    public function read_user($arrArgument) {
        return $this->bll->read_user_BLL($arrArgument);
    }
    public function CALL($arrArgument) {
        return $this->bll->CALL_BLL($arrArgument);
    }

    public function log_login($arrArgument){
        return $this->bll->log_login_BLL($arrArgument);
    }

    public function select_user($arrArgument){
        return $this->bll->select_user_BLL($arrArgument);
    }
    public function activar($arrArgument){
        return $this->bll->activar_BLL($arrArgument);
    }
}