<?php
class controller_login {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "login";
    }
    function validate_key() {
      $arrArgument = $_POST['data'];
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events-Angular/modules/users/model/model/';
      $arrValue = loadModel($path_model, "users_model", "vallog_login", $arrArgument);
      if ($arrValue["rows"] != 0) {
        echo json_encode($arrValue);
      }else
        echo "false";
    }
}
