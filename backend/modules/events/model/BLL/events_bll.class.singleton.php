<?php
/*define('MODEL_PATH', SITE_ROOT . '/model/');
require(MODEL_PATH . "db.class.singleton.php");
require(DAO_EVENTS . "event_dao.class.singleton.php");*/
class events_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = events_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function select_event_BLL($arrArgument){
      return $this->dao->select_event_DAO($this->db, $arrArgument);
    }

    public function check_exist_BLL($arrArgument){
        return $this->dao->check_exist_DAO($this->db, $arrArgument);
    }

    public function search_events_keyword_BLL($arrArgument){
        return $this->dao->search_events_keyword_DAO($this->db, $arrArgument);
    }

    public function mostview_events_BLL($arrArgument){
        return $this->dao->mostview_events_DAO($this->db, $arrArgument);
    }

    public function count_eventdata_BLL($arrArgument){
        $type = "count";
        return $this->dao->check_exist_DAO($this->db, $arrArgument,$type);
    }

    public function obtain_countries_BLL($url){
      return $this->dao->obtain_countries_DAO($url);
    }

    public function obtain_countries_BLL2(){
      return $this->dao->obtain_countries_DAO2();
    }

    public function obtain_provinces_BLL(){
      return $this->dao->obtain_provinces_DAO();
    }

    public function obtain_cities_BLL($arrArgument){
      return $this->dao->obtain_cities_DAO($arrArgument);
    }
}