<?php
//require(BLL_EVENTS . "events_bll.class.singleton.php");

class event_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = events_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_events($arrArgument) {
        return $this->bll->create_events_BLL($arrArgument);
    }

    public function select_event($arrArgument){
        return $this->bll->select_event_BLL($arrArgument);
    }

    public function search_events_keyword($arrArgument){
        return $this->bll->search_events_keyword_BLL($arrArgument);
    }

    public function mostview_events($arrArgument){
        return $this->bll->mostview_events_BLL($arrArgument);
    }

    public function count_eventdata($arrArgument) {
        return $this->bll->count_eventdata_BLL($arrArgument);
    } 

    public function obtain_countries($url){
        return $this->bll->obtain_countries_BLL($url);
    }

    public function obtain_countries2(){
        return $this->bll->obtain_countries_BLL2();
    }

    public function obtain_provinces(){
        return $this->bll->obtain_provinces_BLL();
    }

    public Function obtain_cities($arrArgument){
        return $this->bll->obtain_cities_BLL($arrArgument);
    }

}