<?php
//echo json_encode("products_dao.class.singleton.php");
//exit;

class events_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_events_DAO($db, $arrArgument) {
        $nombre = $arrArgument['name'];
        $propietario = $arrArgument['owner'];
        $dirigido_a = $arrArgument['recommended'];
        $presupuesto = $arrArgument['value'];
        $fecha = $arrArgument['date'];
        $country = $arrArgument['country'];
        $province = $arrArgument['province'];
        $poblac = $arrArgument['poblac'];
        $ticket_price = $arrArgument['ticket_price'];
        $img_event = $arrArgument['0'];

        
        $sql = "INSERT INTO events_info (nombre, propietario, dirigido_a, presupuesto,fecha, country, province, poblac, ticket_price, img_event) "
                . " VALUES ('$nombre', '$propietario','$dirigido_a', '$presupuesto', '$fecha', '$country','$province', '$poblac', '$ticket_price', '$img_event')";
        return $db->execute($sql);
    }

    public function select_event_DAO($db, $arrArgument,$type = "exist" ) {
      if ($arrArgument['type']) {
        switch ($arrArgument['type']) {
          case 'normal':
            $sql = "SELECT * FROM $arrArgument[table] ";
            $e = 0;

            if ($arrArgument['data'] == null) {
              $sql = "SELECT * FROM $arrArgument[table]";
            }else {
               foreach($arrArgument['data'] as $key => $val){
                if ($e == 0) {
                  $sql .= "WHERE $key LIKE '$val' ";
                  $e++;
                }else{
                  $sql .= "AND $key LIKE '$val' ";
                }
              } 
            }

            $rows = $db->execute($sql)->num_rows;

            if ($rows > 0) {
              $user = $db->list_db($db,$sql);
              $exists = true;
              $something = array("exists"=>true,"rows"=>$rows,"events"=>$user);
            }else {
              $something = array("exists"=>false,"error"=>"not exists");
            }
            
            return $something;

            break;
          default:
            echo "fail|ERROR no exist";die;
            break;
        }
      }else {
        echo "fail|ERROR no type function";die;
      }
    }

    public function mostview_events_DAO($db, $arrArgument) {
      $limit = $arrArgument["limit"];
      $type = $arrArgument["type"];

      $sql = "SELECT $type,count($type) FROM events_info WHERE $type NOT LIKE '' or NOT NULL GROUP BY $type ORDER BY count($type) DESC LIMIT 0,$limit";
      return $db->list_db($db,$sql);
    }

    public function obtain_countries_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }
    public function obtain_countries_DAO2(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Project_Events-Angular/backend/resources/ListOfCountryNamesByName.xml');

          $result = $provincias->xpath("/ArrayOftCountryCodeAndName/tCountryCodeAndName/sName | /ArrayOftCountryCodeAndName/tCountryCodeAndName/sISOCode");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'sISOCode' => (string) $result[$i], 'sName' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }
    public function obtain_provinces_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Project_Events-Angular/backend/resources/provinciasypoblaciones.xml');

          $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }

    public function obtain_cities_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Project_Events-Angular/backend/resources/provinciasypoblaciones.xml');
          $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");
          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }
}
