<?php

class controller_events {
    function __construct() {
        //include(UTILS_EVENTS . "utils.inc.php");
        require_once(UTILS."upload.inc.php");
        $_SESSION['module'] = "events";
    }
    function insert_json() {
      if (empty($_SESSION['result_prodpic'])){
		      $_SESSION['result_prodpic'] = array('result' => true, 'error' => "", "data" => "/Project_Events-Angular/media/default-avatar.png");
		}
		$event_pic = $_SESSION['result_prodpic'];
		
		$arrArgument = $_POST['event'];
		array_push($arrArgument, $event_pic['data']);
		$arrValue = false;
      	$path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events-Angular/modules/events/model/model/';
     	$arrValue = loadModel($path_model, "event_model", "create_events", $arrArgument);
   	
     	if ($arrValue){
	        $message = "Event has been successfull registered";
	    }else{
	        $message = "Problem ocurred registering a event";
	    }

	    $_SESSION['event'] = $arrArgument;
	    $_SESSION['message'] = $message;
	    $callback="../../events/results_events";

	    $jsondata['success'] = true;
	    $jsondata['redirect'] = $callback;
	    echo json_encode($jsondata);
    }
	function give_like() {
      $arrValue = false;
      $path_model = MODEL_USERS;
      $arrArgument['call'] = "give_like($_POST[us_],$_POST[ev_])";
      /*
			This function is calling a user_model
			becouse the function CALL is general,
			but is there, i should change this.
      */
      $arrValue = loadModel($path_model, "users_model", "CALL", $arrArgument);
      echo json_encode($arrValue);
    }
    function select_evet() {
    	$data = $_POST['data'];
		
		$arrArgument = array("type"=>"normal","data"=>$data);
        $arrArgument['table'] = "events_table";

        $path_model = MODEL_EVENTS;
        $event = loadModel($path_model, "event_model", "select_event", $arrArgument);
        
        if ($event['exists'] == true) {
          echo json_encode($event);
        }else
          echo "false|The event doesnt exist";die;
    }
    
    function mostview_events() {
      $type = $_POST['type'];
      $limit = $_POST['limit'];

      $arrArgument = array('type' => $type, 'limit' => $limit);

      $arrValue = false;
      $path_model = MODEL_EVENTS;
      $arrValue = loadModel($path_model, "event_model", "mostview_events", $arrArgument);
      if ($arrArgument == "not exist") {
        echo json_encode("error");
      }else
        echo json_encode($arrValue);
    }
    function load() {
    	$jsondata = array();
	    if (isset($_SESSION['event'])) {
	        $jsondata["event"] = $_SESSION['event'];
	    }
	    if (isset($_SESSION['message'])) {
	        $jsondata["message"] = $_SESSION['message'];
	    }
	    close_session();
	    echo json_encode($jsondata);
    }

	function upload() {
		$result_prodpic = upload_files();
  		$_SESSION['result_prodpic'] = $result_prodpic;
  		echo json_encode($result_prodpic);
	}

	function delete() {
		$_SESSION['result_prodpic'] = array();
	    $result = remove_files();
	    if($result === true){
	      echo json_encode(array("res" => true));
	    }else{
	      echo json_encode(array("res" => false));
	    }
	}

	function load_country() {
		$json = array();
    	$url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
		$path_model='/var/www/html/Project_Events-Angular/backend/modules/events/model/model/';
		$json = loadModel($path_model, "event_model", "obtain_countries2");
		if($json){
			echo json_encode($json);
			exit;
		}else{
			$json = "error";
			echo $json;
		}
	}

	function load_city() {
		$jsondata = array();
        $json = array();

		$path_model='/var/www/html/Project_Events-Angular/backend/modules/events/model/model/';
		$json = loadModel($path_model, "event_model", "obtain_cities", $_POST['idPoblac']);
		
		if($json){
			$jsondata["poblacion"] = $json;
			echo json_encode($jsondata);
		}else{
			$jsondata["poblacion"] = "error";
			echo json_encode($jsondata);
		}
	}
	function load_provinces() {
		$jsondata = array();
        $json = array();

		$path_model='/var/www/html/Project_Events-Angular/backend/modules/events/model/model/';
		$json = loadModel($path_model, "event_model", "obtain_provinces");
		if($json){
			$jsondata["provincias"] = $json;
			echo json_encode($jsondata);
		}else{
			$jsondata["provincias"] = "error";
			echo json_encode($jsondata);
		}
	}
	function get_location() {
      $place = $_GET['param'];
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=".$place."&key=AIzaSyBpHhlZbF2vhXzFC0XRhjsmQOu5YE4YlDk",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "cache-control: no-cache"
        ),
      ));
      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);
      echo($response);     
    } 
}
function close_session() {
	    unset($_SESSION['event']);
	    unset($_SESSION['message']);
	    $_SESSION = array();
	    session_destroy();
	}