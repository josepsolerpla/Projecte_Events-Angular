<?php
class controller_contact {
    function __construct() {
        //include(UTILS_HOMEPAGE . "utils.inc.php");
        $_SESSION['module'] = "contact";
    }

    function load_contact() {
      require_once(VIEW_PATH_INC . "header.html");
      require_once(VIEW_PATH_INC . "menu.html");

      loadView('modules/contact/view/', 'contact.html');

      require_once(VIEW_PATH_INC . "footer.html");
    }
    function send_mail(){
      $data = array("name"=>$_POST['inputName']);
      if (filter_var($data['name'], FILTER_VALIDATE_EMAIL)) {
        $email = $data['name'];        
      }else{
        $arrArgument = array("type"=>"normal","data"=>$data);
        $arrArgument['table'] = "users_infoallow";
        $path_model = $_SERVER['DOCUMENT_ROOT'] . '/Project_Events-Angular/backend/modules/users/model/model/';
        $user = loadModel($path_model, "users_model", "select_user", $arrArgument);
        if ($user['exists'] == true) {
          $email = $user["user"]["email"];
          if ($email == "") {
            echo "false|Error the email is not seted";die;
          }
        }else
          echo "false|The user doesnt exist";die;
      }

      if ($user["exists"] == true || $email != "") {
        $arrArgument = array(
          'type' => 'contact',
          'token' => '',
          'inputName' => $_POST['inputName'],
          'inputEmail' => $email,
          'inputSubject' => $_POST['inputSubject'],
          'inputMessage' => $_POST['inputMessage']
        );
        set_error_handler('ErrorHandler');
        try{
            $json = send_mailgun($arrArgument);
            echo "true|Tu mensaje ha sido enviado correctamente";
          } catch (Exception $e) {
              echo "false|Error en el servidor. Intentelo más tarde...";
          }
          restore_error_handler();
        }else
        echo "false|Error not exists";
    } 
}