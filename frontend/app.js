var project_event = angular.module('project_event',['ngRoute','ngCookies','ui.bootstrap','ngMaterial','ngMessages','ngMdIcons']);
project_event.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                .when("/", {templateUrl: "frontend/modules/homepage/view/homepage.view.html"
                    , controller: "HP_controller"
                    , resolve: {
                        events: function (services) {
                            return services.get('events', 'select_evet');
                        },
                        citys: function (services) {
                            return services.post('events','mostview_events',{"limit":3,"type":"province"});
                        },
                        likes: function (likeService) {
                            return likeService.getLikes();
                        }
                    }
                })
                .when("/contact", {
                    templateUrl: "frontend/modules/contact/view/contact.view.html", 
                    controller: "contactCtrl"
                })
                .when("/events/create", {
                    templateUrl: "frontend/modules/events/view/create_events.view.html", 
                    controller: "CeventsCtrl",
                    resolve: {
                        events: function (services) {
                            return services.get('events', 'select_evet');
                        }
                    }
                })
                .when("/events/list", {
                    templateUrl: "frontend/modules/events/view/list_events.view.html", 
                    controller: "LeventsCtrl",
                    resolve: {
                        events: function (services) {
                            return services.get('events', 'select_evet');
                        }
                    }
                })
                .when("/events/:id", {
                    templateUrl: "frontend/modules/events/view/event.view.html",
                    controller: "detailsCtrl",
                    resolve: {
                        event: function (services, $route , $location) {
                            services.post('events', 'select_evet', {data:{"id_event":$route.current.params.id}}).then(function (res) {
                                if (res == "false|The event doesnt exist") {$location.path("/")};
                            });
                            return services.post('events', 'select_evet', {data:{"id_event":$route.current.params.id}});
                        }
                    }
                })
                .when("/user/profile",{
                    templateUrl: "frontend/modules/users/view/user_profile.view.html",
                    controller: "userProfileCtrl",
                })
                .when("/user/activar/:token", {
                    templateUrl: "frontend/modules/homepage/view/homepage.view.html",
                    controller: "verifyCtrl",
                })
                .otherwise("/", {
                    templateUrl: "frontend/modules/homepage/view/homepage.view.html"
                    , controller: "HP_controller"
                    , resolve: {
                        events: function (services) {
                            return services.get('events', 'select_evet');
                        },
                        citys: function (services) {
                            return services.get('events','mostview_events');
                        }
                    }
                });
    }]);