project_event.factory("eventService", function ($rootScope , $q,load_pais_prov_poblac,services) {
    var service = {};
    service.load_selects = load_selects;
    service.load_dropzone = load_dropzone;
    service.giveRate = giveRate;
    
    return service;

    function load_selects($q,$scope) {
        var deferred = $q.defer();
        load_pais_prov_poblac.load_pais()
            .then(function (response) {
                if(response.success){
                    $scope.paises = response.datas;
                }else{
                    $scope.AlertMessage = true;
                    $scope.events.pais_error = "Error al recuperar la informacion de paises";
                    $timeout(function () {
                        $scope.events.pais_error = "";
                        $scope.AlertMessage = false;
                    }, 2000);
                }
                deferred.resolve(response);
            });
        $scope.resetPais = function () {
            if ($scope.events.pais.sISOCode == 'ES') {
                load_pais_prov_poblac.loadProvincia()
                .then(function (response) {
                    if(response.success){
                        $scope.provincias = response.datas;
                    }else{
                        $scope.AlertMessage = true;
                        $scope.events.prov_error = "Error al recuperar la informacion de provincias";
                        $timeout(function () {
                            $scope.events.prov_error = "";
                            $scope.AlertMessage = false;
                        }, 2000);
                    }
                    deferred.resolve(response);
                });
                $scope.poblaciones = null;
            }
        };

        $scope.resetValues = function () {
            var datos = {idPoblac: $scope.events.provincia.id};
            load_pais_prov_poblac.loadPoblacion(datos)
            .then(function (response) {
                if(response.success){
                    $scope.poblaciones = response.datas;
                }else{
                    $scope.AlertMessage = true;
                    $scope.events.pob_error = "Error al recuperar la informacion de poblaciones";
                    $timeout(function () {
                        $scope.events.pob_error = "";
                        $scope.AlertMessage = false;
                    }, 2000);
                }
                deferred.resolve(response);
            });
        };
        return deferred.promise;
    }//end function
    function load_dropzone($q,$scope) {
        $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=events&function=upload',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
                console.log(response);
                response = JSON.parse(response);
                if (response.result == true) {
                    $scope.events.avatar = response.data;
                } else {
                    //$(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    //$('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("user", "delete_avatar", JSON.stringify({'filename': data}));
                }
            }
        }};
    }
    function giveRate(id_u,id_e,rate) {
        var deferred = $q.defer();
            var data = {"id_user":id_u,"id_event":id_e,"rating":rate};
            services.post("users","giverate",data).then(function (resp){
                console.log("rate : ", resp[0]);
                deferred.resolve(resp);
            })
        return deferred;
    }
});
