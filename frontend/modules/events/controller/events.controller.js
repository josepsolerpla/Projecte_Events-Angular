project_event.controller('CeventsCtrl', 
    function ($rootScope,CommonService,$location,$scope, events , dateService , services , load_pais_prov_poblac , eventService , $q , $uibModal) {
    /*
        Load Events data on array to use on 
        validateion,etc
    */
    $scope.events =  {};
	var names = [];
	events.events.forEach(function (res) {
		names.push(res.nombre);
	});
    /*
        Llistat de components del event
    */
    $rootScope.components = []; 
	
    /*
        Functions for validate data,
        using CommonService functions
    */
	$scope.validatename = function() {
        return CommonService.validateArrayValue($scope.events.inputName,names);
	}
    $scope.validatenumber = function(number) {
        return CommonService.validateNumber(number);
    }
    /*
        Validate Date
    */
    dateService.date($scope);
    $scope.valDate = function(){
        return dateService.date();
    }

    /*
        Function to create Event
        you can only use this if all are validated
    */    
    $scope.createEvent = function () {
        console.log($scope.events);
    }

    /*
        Loading the Selects for the Location
    */
    eventService.load_selects($q,$scope);
    eventService.load_dropzone($q,$scope);
    /*
        Add component to the event
    */
    $scope.addComp = function () {
        var modalInstance = $uibModal.open({
            animation: 'true',
            templateUrl: 'frontend/modules/events/view/addComponent.view.html',
            controller: 'addComponent',
            size: "lg",
            resolve: {
                        usernames: function (services) {
                            var data = {"table":"users_infoallow"};
                            return services.post('users', 'select_user', data);
                        },
                    }
        });
    };
    /*
        Delete Component From array
    */
    $scope.delComp = function(id) {
        var index = null;
        $rootScope.components.forEach(function (res,ind){
            if (res.id == id) {
                index = ind;
            }
        })
        $rootScope.components.splice(index, 1);
    }
});

project_event.controller('addComponent', function ($rootScope,$scope,usernames,$uibModalInstance) {
    /*
        Load usernames avoiding the 
        already loaded on rootScope
    */
    $scope.usernames =  usernames.user.filter(function(item1) {
                          for (var i in $rootScope.components) {
                            if (item1.id === $rootScope.components[i].id) { return false; }
                          };
                          return true;
                        });
    /*
        Close Modal
    */
    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };
    /*
        Add component To array,
        its showed below the button
    */
    $scope.addComponent = function (user) {
        $rootScope.components.push(user);
        $scope.close();
    }

})

project_event.controller('LeventsCtrl', function ($scope, events,CommonService,cookiesService,eventService) {
    console.log("events", events);
    /*
        Load pagination config
    */
    $scope.event = events.events;
    CommonService.loadConfigList($scope);
    /*
        Function to set the displayed items when change page,
    */
    $scope.pageChanged = function() {
      var startPos = ($scope.currentPage - 1) * 3;
      $scope.displayItems = $scope.event.slice(startPos, startPos + 3);
    };


    /*
        Part del examen
    */

    $scope.rate = function () {
        var user = cookiesService.GetCredentials();
        if (user) {
            CommonService.banner("Gracias por dar "+this.rating+" estrellas");
            eventService.giveRate(user.usuario,this.of.id_event,this.rating);
        }else{
            CommonService.banner("Tienes que estar logeado");
            this.rating = 0;
        }
    }
});

project_event.controller('detailsCtrl', function ($scope , event , services , events_map ) {
    /*
        Load map and marker
        if not exist get the position via api,
        in case of error set "No map detected"
    */
    event = event.events[0]
    $scope.event = event;
    $scope.markers = [];
    $scope.error = [];
    if (event.province) {
        /*
            Take location from api
        */
        services.get('events', 'get_location', event.province ).then(function (res) {
            var location = res.results[0].geometry.location;
            event.lat = location.lat;
            event.lng = location.lng;
            setTimeout(function(){ events_map.cargarmapEvent(event, $scope); }, 10 );
        });
    }else {
        if (event.lat && event.lng) {
            setTimeout(function(){ events_map.cargarmapEvent(event, $scope); }, 10 );
        }else{
            $scope.error.nomap = "No map detected";            
        }
    };
});