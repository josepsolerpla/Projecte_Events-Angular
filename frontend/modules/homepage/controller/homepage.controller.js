project_event.controller('HP_controller', 
  function ($http , $scope, events , citys , services ,$timeout,homepageService,CommonService,likeService,likes) {
                                                           /*	LOAD EVENTS	*/
  /*
    $Scopes configuration for the pagination
  */
  $scope.event = events.events;
  CommonService.loadConfigList($scope);
  /*
    Function to set the displayed items when change page,
  */
  $scope.pageChanged = function() {
    var startPos = ($scope.currentPage - 1) * 3;
    $scope.displayItems = $scope.event.slice(startPos, startPos + 3);
  };
                                                           /*	CHANGE VIEW	*/
  $scope.view = "frontend/modules/homepage/view/normal.view.html";
  $scope.Tview = {
    cb1 : true
	};
  /*
    Change between the two types of views:
      Scrolled  OR  Pagination(normal)
  */
	$scope.Cview = function (res) {
		if (res) {
			$scope.view = "frontend/modules/homepage/view/scroll.view.html";
		}else{
			$scope.view = "frontend/modules/homepage/view/normal.view.html";
		}
	}
	                                                         /*	MOST VIEW	*/
	$scope.data = {city : ""};
	$scope.cityData = [];
  /*
   Load images on Most View Checkbox 
   */
  homepageService.loadImage(citys,$scope);
  /*
    Function when checkbox is changed,
    the value is taked at the same time on the filter
  */
  $scope.changecity = function () { 
	}
                                                           /*  SCROLL VIEW   */
  $scope.items = $scope.event.slice(0, 3);

  $scope.loading = false;
  $scope.CP = 1;
  /*
    Funtion to load more events on scroll down
  */
  $scope.more = function(){
    homepageService.loadMore($scope);
  };
  /*
    Function to give a like
  */
  if (likes) {
	  $scope.doneLike = likes;
	  console.log("likes", $scope.doneLike);
  }
  
  $scope.like = function() {
    likeService.like(this.of.id_event);
  }

});