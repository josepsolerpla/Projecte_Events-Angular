project_event.factory("homepageService", function ($rootScope , $q,load_pais_prov_poblac,$http) {
    var service = {};
    service.loadImage = loadImage;
    service.loadMore = loadMore;
    return service;
    /*
        Function to grab the images from the url or just set
        the default image.
    */
    function loadImage(citys,$scope) {
        citys.forEach(function (res) {
            var url = "/Project_Events-Angular/backend/media/"+res.province+".jpg";
            $http({
                url:url
            }).success(function() { 
                var value = {"id":url,"title":res.province,"value":res.province};
                $scope.cityData.push(value);
            }).error(function() { 
                var value = {"id":"/Project_Events-Angular/backend/media/default-city.jpg","title":res.province,"value":res.province};
                $scope.cityData.push(value);
            })
        })
    }
    /*
        Function for load more events on the scroll
    */
    function loadMore($scope) {
        $scope.loading = true;  
        if ($scope.CP*3 <= $scope.event.length) {
          $scope.CP += 1;
          var startPos = ($scope.CP - 1) * 3;
          object = $scope.event.slice(startPos, startPos + 3);
            object.forEach(function (res){
              $scope.items.push(res);
          })
        };
    }
});
