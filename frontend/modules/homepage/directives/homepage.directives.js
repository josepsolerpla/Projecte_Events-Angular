/*
  Directive to detect when the scroll is at the bottom
  of the page.
*/
project_event.directive("whenScrolled", function($document){
  return{
    restrict: 'A',
    link: function(scope, elem, attrs){
      raw = elem[0];
      var doc = $document[0].scrollingElement;
      angular.element($document).bind("scroll", function() {
        if (doc.scrollTop + doc.offsetHeight >= doc.scrollHeight) {
          scope.loading = false;
          scope.$apply(attrs.whenScrolled);
        }
      });
    }
  }
});