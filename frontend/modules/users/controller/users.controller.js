project_event.controller('menuCtrl', function ($scope, $uibModal, UserService, $rootScope, $anchorScroll) {
    UserService.login();
    $rootScope.bannerV = false;
    $rootScope.bannerText = "";

    $rootScope.changeAcc = function(user) {
        UserService.changeAcc(user);
    }
    $scope.open = function () {
        var modalInstance = $uibModal.open({
            animation: 'true',
            templateUrl: 'frontend/modules/users/view/modal.view.html',
            controller: 'modalWindowCtrl',
            size: "lg",
            resolve: {
                        usernames: function (services) {
                            var data = {"table":"users_infoallow"};
                            return services.post('users', 'select_user', data);
                        },
                    }
        });
    };
    $scope.logout = function () {
        UserService.logout();
    }
    $scope.toTheTop = function () {
        $anchorScroll();
    };
});

project_event.controller('modalWindowCtrl',
    function ($rootScope,$scope,$uibModalInstance,facebookService,twitterService,normalService,usernames) {
    $rootScope.usernames = usernames;
    $scope.form = {
            user: "",
            pass: ""
        };
    $scope.f_login = true;
    $scope.error ={};

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
        $scope.f_password = false;
        $scope.register   = false;
        $scope.f_login    = true;
    };
    $rootScope.closeLogin = function () {
        $scope.close();
    }
    $scope.login = function () {
        normalService.login($scope);
    };

    $scope.loginTw = function () {
        twitterService.login($scope);
    };

    $scope.loginFb = function () {
        facebookService.login($scope);
    };

    $scope.valUser = function() {
        var input = $scope.form.user;
        var back = true;
        usernames.user.forEach(function (res){
            if (res.name == input || res.email == input) {
                back = false;
            };
        });
        return back;
    }
});

project_event.controller('signupCtrl',
 function ($rootScope,UserService,$location) {
    $rootScope.signup = {
        inputUsername  : "",
        inputEmail     : "",
        inputPassword  : "",
        inputRPassword : "",
    };

    $rootScope.registerUser = function () {
        UserService.register();
        $location.path('/');
    }

    $rootScope.valSUser = function () {
        var back = true;
        var input = $rootScope.signup.inputUsername;
        $rootScope.usernames.user.forEach(function (res){
            if (res.name == input || res.email == input) {
                back = false;
            };
        });
        return back;
    }
});

project_event.controller('changepassCtrl',
 function ($scope) {

});
project_event.controller('verifyCtrl', function (UserService, $location, $route, services, cookiesService,CommonService) {
    var get = $route.current.params.token;
    var token = get.split("~")[0];
    var id_user = get.split("~")[1];

    if (!token && !id_user) {
        CommonService.banner("Ha habido algún tipo de error con la dirección", "Err");
        $location.path('/');
    }
    var data = {"token":token,"id_user":id_user}
    services.post("users", "activar", data).then(function (response) {
        console.log("response", response);
        if (response.success) {
            var cookie = {
                    token: response.token,
                    user: {id:response.user.id,
                        user_type:response.user.user_type,
                        name:response.user.name,
                        avatar:response.user.photo
                    }
                }
            CommonService.banner("Su cuenta ha sido satisfactoriamente verificada", "");
            cookiesService.SetCredentials(cookie);
            UserService.login();
            $location.path('/');
        } else {
            if (response.datos == 503){
                CommonService.banner("Error, intentelo mas tarde", "Err");
                $location.path("/");
            }else if (response.error == 404){
                CommonService.banner("Error, intentelo mas tarde", "Err");
                $location.path("/");
            }
        }
    });
});
project_event.controller('userProfileCtrl',
 function ($scope,cookiesService,$rootScope,services,$location,UserService) {
    var user = cookiesService.GetCredentials();
    if (user) {
        $rootScope.user = {
            email: undefined,
            name: undefined,
            method: true,
            username: undefined,
            avatar: undefined
        }

        $scope.UForm = false;

        $scope.update_user = function () {
            var data = {"id":user.usuario,'change':$scope.update};
            UserService.updateUser(data);
        }
        UserService.getUser();
        UserService.loadRatings();
    }else {
        $location.path("/");
    }
});