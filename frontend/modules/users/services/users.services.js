project_event.factory("UserService", ['$location', '$rootScope', 'services', 'cookiesService','$q','$location','CommonService',
function ($location, $rootScope, services, cookiesService,$q,$location,CommonService) {
        var service = {};
        service.login = login;
        service.logout = logout;
        service.changeAcc = changeAcc;
        service.register = register;
        service.getUsers = getUsers;
        service.updateUser = updateUser;
        service.getUser = getUser;
        service.loadRatings = loadRatings;
        return service;

        function changeAcc(res) {
            if (res) {
                var user = cookiesService.GetCredentials();

                var response = {
                    token: user.token,
                    user: {id:res.id,
                        user_type:res.user_type,
                        name:res.name,
                        avatar:res.photo
                    }
                }

                cookiesService.SetCredentials(response);
                $rootScope.jOLogin = true;
                if ($location.$$path == "/user/profile") {
                    getUser();
                }
                login();
            };
        }

        function login() {
            var user = cookiesService.GetCredentials();
            if (user) {
                var data = {"id_user":user.usuario,"token":user.token};
                services.post('users','validate_key',data).then(function (op) {
                    if (op == "true") {
                        $rootScope.accederV = false;
                        $rootScope.profileV = true;
                        $rootScope.logoutV = true;
                        $rootScope.myeventsV = true;
                        $rootScope.favoriteV = true;

                        var data = {id:user.usuario};
                        services.post('users','get_account',data).then(function (res) {
                            if ($rootScope.jOLogin == true) {
                                CommonService.banner("Se han cargado los datos de : "+user.nombre, "");
                                $rootScope.jOLogin = false;
                            }else{
                                $rootScope.jOLogin = true;
                            }
                            $rootScope.accountsV = true;
                            $rootScope.accountsR = res;
                        });

                        $rootScope.avatar = user.avatar;
                        $rootScope.usuario = user.usuario;
                        $rootScope.nombre = user.nombre;

                        if (user.tipo === "owner") {
                            $rootScope.adminV = false;
                            $rootScope.creatorV = true;
                        } else if (user.tipo === "admin") {
                            $rootScope.adminV = true;
                            $rootScope.creatorV = true;
                        } else {
                            $rootScope.adminV = false;
                            $rootScope.creatorV = false;
                        }
                    }
                });
            } else {
                $rootScope.accederV = true;
            }
        }

        function logout() {
            cookiesService.ClearCredentials();

            $rootScope.accederV = true;
            $rootScope.profileV = false;
            $rootScope.myeventsV = false;
            $rootScope.favoriteV = false;
            $rootScope.accountsV = false;
            $rootScope.jOLogin = true;

            $rootScope.avatar = '';
            $rootScope.nombre = '';

            $rootScope.adminV = false;
            $rootScope.misofertasV = false;

            $rootScope.logoutV = false;
            $location.path("/");
        }
        function register() {
            var deferred = $q.defer();
                var user = $rootScope.signup;
                services.post('users','insert_into',user).then(function (res) {
                    console.log("res", res);
                    if (res.exists == true) {
                        $rootScope.closeLogin();
                        CommonService.banner("El usuario se ha dado de alta correctamente, revisa su correo para activarlo", "");
                    }else {
                        CommonService.banner("Error en el servidor", "Err");
                    }
                });
            return deferred.promise;
        }
        function getUsers() {
            var deferred = $q.defer();
                var data = null;
                services.post('users', 'select_user', data).then(function (res) {
                    $rootScope.users = res;
                    CommonService.banner("Se han cargado sus cuentas asociadas a este correo", "");
                    deferred.resolve(res);
                })
            return deferred.promise;
        }
        function getUser(){
            var user = cookiesService.GetCredentials();
            var data = {"id":user.usuario};
            services.post("users","get_user",data).then(function (response) {
                console.log("response", response);
                $rootScope.user.email = response.email;
                $rootScope.user.password = response.password;
                $rootScope.user.phone = response.phone;
                $rootScope.user.surname = response.surname;
                $rootScope.user.name = response.last_name;
                $rootScope.user.method = response.method;
                $rootScope.user.username = response.name;
                $rootScope.user.avatar = response.photo;
                $rootScope.update = {
                    password: response.password,
                    name: response.name,
                    phone: response.phone,
                    surname: response.surname,
                };
            })
        }
        function loadRatings(){
            var user = cookiesService.GetCredentials();
            var data = {"search_factor":{"id_user":user.usuario},"table":"rating_users"};
            services.post("users","select_user",data).then(function (resp){
                $rootScope.ratings = resp.user;
            })
        }
        function updateUser(user){
            var deferred = $q.defer();
                var data = user;
                services.post('users', 'update_user', data).then(function (res) {
                    console.log("res", res);
                    CommonService.banner("Se han actualizado los datos", "");
                    deferred.resolve(res);
                })
            return deferred.promise;
        }
}]);
