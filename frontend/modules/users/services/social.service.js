project_event.factory('iniSocial', function ($q) {
    var service = {};
    service.ini = ini;
    return service;

    function ini() {
        var deferred = $q.defer();
            var config = {
                apiKey: "AIzaSyBCj86CzvDOS_mt_OnJg4yNNZ_yEZy339c",
                authDomain: "events-angular.firebaseapp.com",
                databaseURL: "https://events-angular.firebaseio.com",
                projectId: "events-angular",
                storageBucket: "events-angular.appspot.com",
                messagingSenderId: "565442458369"
            };
            if (!firebase.apps.length) {
             firebase.initializeApp(config);
            }
            var authService = firebase.auth(); 
            deferred.resolve(authService);
        return deferred.promise;
    }
})
project_event.factory('normalService', function ($q,$rootScope,services,cookiesService,UserService,CommonService) {
    var service = {};
    service.login = login;
    service.logout = logout;
    return service;

    function login($scope) {
        var deferred = $q.defer();
            var search_factor = {"name":$scope.form.user,"password":$scope.form.pass};
            var data = {"search_factor": search_factor, "logs": true};
            services.post('users', 'check_exist', data).then(function (response) {
                console.log(response);
                if (response.exists == true) {
                    $scope.close();
                    $scope.err_password = false;
                    $scope.err_login = false;
                    cookiesService.SetCredentials(response);
                    UserService.login();
                }else {
                    if (response.error == "worng password") {
                        $scope.errorpass = "Wrong password";
                        $scope.err_password = true;
                    };
                    $scope.err_login = true;
                    $scope.error.login = response.error;
                }
                deferred.resolve(response);
            });  
        return deferred.promise;
    }
})
project_event.factory('twitterService', function ($q,services,cookiesService,UserService,iniSocial,CommonService) {
    var service = {};
    service.login = login;
    service.logout = logout;
    return service;

    function login($scope) {
        var deferred = $q.defer();
            iniSocial.ini().then(function (authService) {
                var providerT = new firebase.auth.TwitterAuthProvider();
                authService.signInWithPopup(providerT)
                  .then(function(result) {
                    var user = result.additionalUserInfo.profile;
                    var data = {"method":"twitter","id":user.id,"name":user.name,"last_name":user.screen_name,"email":"","photo":user.profile_image_url};
                    services.post('users', 'insert_into_firebase', data).then(function (response) {
                        if (response.exists == true) {
                            var response = {token: response.token,user: {id:response.user.id_user,user_type:response.user.type,name:response.user.name,avatar:response.user.photo}}
                            $scope.close();
                            $scope.err_password = false;
                            $scope.err_login = false;
                            cookiesService.SetCredentials(response);
                            UserService.login();
                        }else {
                            CommonService.banner("Error server, intentelo mas tarde", "Err");
                        }
                      deferred.resolve(result);
                    });
                  }).catch(function(error) {
                      console.log('Detectado un error:', error);
                });
            })       
        return deferred.promise;
    };
});

project_event.factory('facebookService', function ($q,services,cookiesService,UserService,iniSocial,CommonService) {
    var service = {};
    service.login = login;
    service.logout = logout;
    return service;

    function login($scope) {
        var deferred = $q.defer();
            iniSocial.ini().then(function (authService) {
                var providerF = new firebase.auth.FacebookAuthProvider();
                authService.signInWithPopup(providerF)
                  .then(function(result) {
                      var user = result.additionalUserInfo.profile;
                      var data = {"method":"facebook","id":user.id,"name":user.first_name,"last_name":user.last_name,"email":user.email,"photo":user.picture.data.url};
                      services.post('users', 'insert_into_firebase', data).then(function (response) {
                        if (response.exists == true) {
                            var response = {token: response.token,user: {id:response.user.id_user,user_type:response.user.type,name:response.user.name,avatar:response.user.photo}}
                            $scope.close();
                            $scope.err_password = false;
                            $scope.err_login = false;
                            cookiesService.SetCredentials(response);
                            UserService.login();
                        }else {
                            CommonService.banner("Error server, intentelo mas tarde", "Err");
                        }
                        deferred.resolve(response);
                    });  
                  })
                  .catch(function(error) {
                      console.log('Detectado un error:', error);
                });
            })       
        return deferred.promise;
    };
});
