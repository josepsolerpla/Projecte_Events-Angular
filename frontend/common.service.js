project_event.factory("CommonService", ['$rootScope','$timeout', function ($rootScope, $timeout) {
        var service = {};
        service.banner = banner;
        service.amigable = amigable;
        service.validateArrayValue = validateArrayValue;
        service.validateNumber = validateNumber;
        service.loadConfigList = loadConfigList;
        return service;

        function loadConfigList($scope) {
            $scope.currentPage = 1;
            $scope.displayItems = $scope.event.slice(0, 3);

            $scope.maxSize = 5;
            $scope.bigTotalItems = 175;
            $scope.bigCurrentPage = 1;
        }

        function banner(message, type) {
            $rootScope.bannerText = message;
            $rootScope.bannerClass = 'alertbanner aletbanner' + type;
            $rootScope.bannerV = true;

            $timeout(function () {
                $rootScope.bannerV = false;
                $rootScope.bannerText = "";
            }, 5000);
        }
        
        function amigable(url) {
            var link = "";
            url = url.replace("?", "");
            url = url.split("&");

            for (var i = 0; i < url.length; i++) {
                var aux = url[i].split("=");
                link += aux[1] + "/";
            }
            return link;
        }
        function validateArrayValue(name,array){
            var back = true;
            array.forEach(function (res){
                if (res == name) {
                    back = false;
                };
            });
            return back;
        }
        function validateNumber(number){
            var back = true;
            if (!isNaN(number)) {
                back = false;
            };
            return back;
        }
}]);
project_event.factory("dateService", ['$rootScope','$timeout', function ($rootScope, $timeout) {
    var service = {};
    service.date = date;
    return service;

    function date($scope,min = 0, max = 20){
        $scope.myDate = new Date();

        $scope.minDate = new Date(
            $scope.myDate.getFullYear() - max,
            $scope.myDate.getMonth(),
            $scope.myDate.getDate()
        );

        $scope.maxDate = new Date(
            $scope.myDate.getFullYear() - min,
            $scope.myDate.getMonth(),
            $scope.myDate.getDate()
        );
    }

    //$scope.format = function(date) {
        /* $scope.format function author: Joan Montes */
    //    $scope.birthdate = $filter('date')(new Date(date), 'MM/dd/yyyy')
    //};
}]);
project_event.factory("likeService", 
 function ($rootScope,cookiesService,services,$q,CommonService) {
    var service = {};
    service.getLikes = getLikes;
    service.like = like;
    return service;

    function like(ev_){
        var us_ = cookiesService.GetCredentials();
        if (us_) {
            var data = {"us_":us_.usuario,"ev_":ev_};
            services.post('events','give_like',data).then(function (res){
                console.log(res); 
            });
        }else{
            CommonService.banner("Tienes que logearte")
        }
    }
    function getLikes(){
        var deferred = $q.defer();
            var us_ = cookiesService.GetCredentials();
            if (us_) {
                var data = {"us_":us_.usuario,"ev_":"'%'"};
                services.post('events','give_like',data).then(function (res){
                    var id_event = [];
                    res.forEach(function (o){
                        id_event.push(o.id_event);
                    })
                    deferred.resolve(id_event); 
                    return id_event;
                });
            }else {
                return false;
            }
        return deferred.promise;
    }
});